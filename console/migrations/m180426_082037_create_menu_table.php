<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m180426_082037_create_menu_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu', [
            'id' => $this->primaryKey(),
            'userID' => $this->integer()->notNull(),
            'productID' => $this->integer()->notNull(),
            'portions' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu');
    }
}
