<?php

use yii\db\Migration;
use common\models\Product;
/**
 * Handles the creation of table `product`.
 */
class m180426_055611_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'price' => $this->decimal(11,2)->notNull(),
            'userPrice' => $this->decimal(11,2)->notNull(),
            'status' => $this->integer()->defaultValue(Product::STATUS_ACTIVE),
            'dateCreated' => $this->integer()->defaultValue(time()),
            'dateEnded' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }
}
