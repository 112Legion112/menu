<?php

use yii\db\Migration;
use common\models\Product;
/**
 * Class m180426_074250_insert_product
 */
class m180426_074250_insert_product extends Migration
{
    const DATA = [
        [
            'name' => 'Cyп',
            'price' => 350,
            'userPrice' => 25,
            'status' => Product::STATUS_ACTIVE,
        ],
        [
            'name' => 'Пица',
            'price' => 400,
            'userPrice' => 35,
            'status' => Product::STATUS_ACTIVE,
        ]
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach (self::DATA as $data) {
            $this->insert('product', $data);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach (self::DATA as $data) {
            $this->delete('product', $data);
        }
    }
}
