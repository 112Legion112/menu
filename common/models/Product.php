<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property string $price
 * @property string $userPrice
 * @property int $status
 * @property int $dateCreated
 * @property int $dateEnded
 */
class Product extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_FINISHED = 2;

    public static function getStatuses(){
        return[
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_FINISHED => Yii::t('app', 'Finished'),
        ];
    }

    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'userPrice'], 'required'],
            [['price', 'userPrice'], 'number'],
            [['status'], 'integer'],
            [['dateCreated','dateEnded'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'userPrice' => Yii::t('app', 'User Price'),
            'status' => Yii::t('app', 'Status'),
            'dateCreated' => Yii::t('app', 'Date Created'),
        ];
    }
}
