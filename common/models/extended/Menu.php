<?php
namespace common\models\extended;
use common\models\Menu as BaseMenu;
/**
 * @property Product $product
 */

class Menu extends BaseMenu
{
    public function getProduct(){
        return Menu::hasOne(Product::class,['id' => 'productID']);
    }
}