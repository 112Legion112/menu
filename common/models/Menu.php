<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $userID
 * @property int $productID
 * @property int $portions
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productID'], 'required'],
            [['userID', 'productID', 'portions'], 'integer'],
            ['portions', 'compare', 'compareValue' => 1, 'operator' => '>='],
            [['userID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'userID' => Yii::t('app', 'User ID'),
            'productID' => Yii::t('app', 'Product ID'),
            'portions' => Yii::t('app', 'Portions'),
        ];
    }
}
