<?php


use backend\models\Product;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php //Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'username',
//            'from',
            [
                'attribute' => 'createTimeRange',
                'filter' => DateRangePicker::widget([
                    'name'=>'createTimeRange',
//                    'useWithAddon'=>true,
//                    'language'=>$config['language'],             // from demo config
                    'hideInput'=>true,           // from demo config
//                    'presetDropdown'=>false, // from demo config
                    'pluginOptions'=>[
                        'locale'=>[
                            'format'=>'YYYY-MM-DD',
                            'separator'=>':',
                        ],
                        'opens'=>'left'
                    ]
                ]),
                'label' => Yii::t('app', 'Range'),
                'value' => function($model){
                    /** @var \backend\models\search\UserSearch $model */
                    if (isset(Yii::$app->request->queryParams['createTimeRange'])) {
                        $range = explode(':', Yii::$app->request->queryParams['createTimeRange']);
                        $products = Product::getProductsByPeriod(strtotime($range[0]), strtotime($range[1]));
                        return $model->calculatePriceProducts($products);
                    }
                },

            ],
//            'email:email',
//            'password_hash',
//            'auth_key',

            //'confirmed_at',
            //'unconfirmed_email:email',
            //'blocked_at',
            //'registration_ip',
            //'created_at',
            //'updated_at',
            //'flags',
            //'last_login_at',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<!--    --><?php //Pjax::end(); ?>
</div>
