<?php

use backend\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'price',
            'userPrice',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function($model, $key, $index, $column){
//                    return $model::getStatuses()[$model->status];
                    return Html::activeDropDownList($model,'status', Product::getStatuses(),
                        [
                            'data-id' => $model->id,
                            'id' => "product-status_id-$model->id",
                            'onchange' => "
                                $.ajax({
                                    url: './product/change-status',
                                    type: 'post',
                                    data: {product_id: $key, status_id: $('#product-status_id-$model->id').val()}                                   
                                });"
                        ]
                    );
                }
            ],
            //'dateCreated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
