<?php

use backend\models\Product;
use dektrium\user\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'userID',
                'label' => Yii::t('app', 'Username'),
                'value' => function($model){
                    return User::find()->where(['id'=>$model->userID])->one()->username;
                }
            ],
            [
                'attribute' => 'productID',
                'label' => Yii::t('app', 'Product name'),
                'value' => function($model){
                    return Product::find()->where(['id'=>$model->productID])->one()->name;
                }
            ],
            'portions',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
