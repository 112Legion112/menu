<?php
namespace frontend\controllers;

use frontend\models\Product;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use frontend\models\Menu;

class MenuController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionCreate(){
        $products = Product::find()->where(['status'=>Product::STATUS_ACTIVE])->all();
        $models = [];
        foreach($products as $product){
            $models[] = new Menu;
        }

        if ( Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models) ) {
            foreach( $models as $model ){
                if ($model->portions != null && $model->portions != 0)
                    $model->save(false);
            }
            $this->redirect('/site');
        }

        foreach($products as $index => $product){
            $models[$index]->productID = $product->id;
        }

        return $this->render('create', [
            'models' => $models,
        ]);
    }
}