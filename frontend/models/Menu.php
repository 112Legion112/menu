<?php
namespace frontend\models;

use yii\behaviors\BlameableBehavior;
use common\models\extended\Menu as BaseMenu;

class Menu extends BaseMenu
{
    public function behaviors(){
        return [
            [
                'class' => BlameableBehavior::class,
                'createdByAttribute' => 'userID',
                'updatedByAttribute' => 'userID'
            ]
        ];
    }
}