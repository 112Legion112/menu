<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var $this yii\web\View
 * @var $models[] frontend\models\Menu
 */
$items = ['' => 0];
for($i = 1; $i <= 10; $i++) {
    $items[$i] = $i;
}
?>
<div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-4 col-md-3">
            Название
        </div>
        <div class="col-xs-4 col-md-3">
            Цена
        </div>
        <div class="col-xs-4 col-md-3">
            Количество
        </div>
    </div>
    <?php foreach($models as $index => $model) {
        /** @var $model \frontend\models\Menu */?>
    <?= $form->field($model, "[$index]productID")->hiddenInput(['value' => $model->productID])->label(false);?>
    <div class="row">
        <div class="col-xs-4 col-md-3">
        <?= $model->product->name?>
        </div>
        <div class="col-xs-4 col-md-3">
            <?= $model->product->userPrice?>
        </div>
        <div class="col-xs-4 col-md-3">
            <?= $form->field($model, "[$index]portions")->dropDownList($items)->label(false) ?>
        </div>
    </div>
    <?php } ?>


    <div class="row">
        <div class="col-xs-4">
        <?= Html::submitButton(Yii::t('app','Save'),['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
