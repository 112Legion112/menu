<?php
/**
 * @var $this yii\web\View
 * @var $models[] frontend\models\Menu;
 */

$this->title = 'Я поел';
?>
<div class="menu-create">
    <div class="body-content">
        <?= $this->render('_form', [
            'models' => $models,
        ]) ?>
    </div>
</div>
